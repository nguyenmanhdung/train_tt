## Hướng dẫn chạy chương trình:

* Setup môi trường và các thứ liên quan <ghi nhớ những điều sau chỉ chạy lần đầu để cài đặt môi trường>:

Đầu tiên cài pycharm để khi visual code hoặc sửa code sẽ clear hơn. Khi cài conda nhớ thêm biến môi trường. Sau khi cài chạy lệnh sau để chắc chắn rằng conda đã được cài:

```commandline
conda --version
```

Tạo môi trường mới có tên là "trai_tt" sử dụng conda và phiên bản python 3.8:

```commandline
conda create --name train_tt python=3.8
```

Activate môi trường "train_tt" ở trên:

```commandline
conda activate train_tt
```

Cài đặt các thư viện liên quan:

```commandline
pip install -r requirements.txt
```

* Chạy chương trình:

```commandline
python Image_Captioning_WebService.py
```


* Chú ý để đường dẫn tới model vào file path_to_model.txt

* Note:
Khi gửi một ảnh tới thì đường dẫn API (dòng thứ 368 trong file yolov5/templates/second_theme.html) là:
http://127.0.0.1:5000/upload
API có dạng: {"image": image_base_64, "yes": number_of_yes, "no": number_of_no, "total": yes+no}