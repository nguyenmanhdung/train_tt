import os.path
import cv2

import os
from Image_Site import Predict
from flask import Flask, request, render_template, send_from_directory

import base64
import json
import matplotlib.pyplot as plt
import argparse

__author__ = ''

app = Flask(__name__)

APP_ROOT = os.path.dirname(os.path.abspath(__file__))

handled_result_image = None


@app.route("/")
def index():
    return render_template("second_theme.html")


@app.route("/upload", methods=["POST"])
def upload():
    target = os.path.join(APP_ROOT, 'images/')
    print(target)
    if not os.path.isdir(target):
            os.mkdir(target)
    else:
        print("Couldn't create upload directory: {}".format(target))
    print("files = ", request.files.getlist("file"))
    for upload in request.files.getlist("file"):
        print(upload)
        print("{} is the file name".format(upload.filename))
        filename = upload.filename
        destination = "/".join([target, filename])
        print("Accept incoming file:", filename)
        print("Save it to:", destination)
        upload.save(destination)
    execution_path = target
    print(execution_path)
    image_result, yes, no, total = Predict(os.path.join(execution_path, filename))

    str_b64_result = base64.b64encode(image_result).decode()
    # print(str_b64)
    dicts = {"img_base64_result": str_b64_result}
    return json.dumps(dicts)

@app.route('/upload/<filename>')
def send_image(filename):
    print("Filename = ##", filename)
    return send_from_directory("images", filename)

if __name__ == "__main__":
    app.run(debug=True)
